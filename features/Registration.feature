Feature: Registration

    @Registration_TC001
    Scenario: As a User I want to registration using invalid phone number So that i can make sure i can not registration
        Given I open the Login page
        When I click the Sign Up link
        And I click the Registration with googlee
        And I input "cindyclaudee37@gmail.com" to the email
        And I click the Berikutnya button
        And I input "cindyclaudee37@gmail.com" to the password
        Then I click the next button
        When I fill “Cindy Claudia S” to field name
        And I fill “cindyclaudia456” to field username
        Then I click the Register button
        When I fill “81178345623213” to field phone number
        Then I click the Send Verification Code button

    @Registration_TC002
    Scenario: As a User I want to registration using invalid verification code So that i can make sure i can not registration
        Given I open the Registration page by google
        When I input the email
        And I fill “Cindy Claudia S” to field name
        And I fill “cindyclaudia456” to field username
        Then I click the Register button
        When I fill “81367534653” to field phone number
        And I click the Send Verification Code button
        Then I see the message that need to check my phone message
        When I click the Verify phone number button
        And I fill “1234”
        And I click Verification button
        Then I see the error message

    @Registration_TC003
    Scenario: As a User I want to registration with using username So that i can make sure i can not registration
        Given I open the Registration page by google
        When I input the email
        And I fill “Cindy Claudia S” to field name
        And I fill “cindyclaudia456” to field username
        Then I click the Register button
        When I fill “81367534653” to field phone number
        And I click the Send Verification Code button
        Then I see the message that need to check my phone message
        When I click the Verify phone number button
        And I fill “1234”
        And I click Verification button
        Then I see the error message

    @Registration_TC004
    Scenario: As a User I want to registration using expired verification code So that i can make sure i can not registration
        Given I open the Registration page by google
        When I input the email
        And I fill “Cindy Claudia S” to field name
        And I fill “cindyclaudia456” to field username
        Then I click the Register button
        When I fill “81367534653” to field phone number
        And I click the Send Verification Code button