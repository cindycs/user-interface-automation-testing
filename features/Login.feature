Feature: Login

    @Login_TC001
    Scenario: As a User I want to login using invalid email and invalid password So that i can make sure i can not login
        Given I open the Login page
        When I fill invalid email “cindyclaudia123@gmail.com” to field email
        And I fill invalid password “12345” to field password
        Then I click Login button

    @Login_TC002
    Scenario: As a User I want to login by not filling in the field email and password So that i can make sure i can not login
        Given I open the Login page
        Then I click Login button

    @Login_TC003
    Scenario: As a User I want to login using uppercase/lowercase valid username So that i can make sure i can login using it
        Given I open the Login page
        When I fill uppercase/lowercase “CINDYCLAUDIA123@GMAIL.COM” to field email
        And I fill valid password “qwerty” to field password
        Then I click Login button
