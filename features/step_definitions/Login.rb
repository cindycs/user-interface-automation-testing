require 'selenium-webdriver'
require 'rubygems'
require 'rspec'

driver = Selenium::WebDriver.for:chrome

Given("I open the Login page") do
  driver.navigate.to "https://stockbit.com/login"
end

When("When I fill invalid email “cindyclaudia123@gmail.com” to field email") do
    driver.find_element(:id, 'username').send_keys("cindyclaudia123@gmail.com")              
    sleep(2)                                                                
end

When("I fill uppercase/lowercase “CINDYCLAUDIA123@GMAIL.COM” to field email") do
    driver.find_element(:id, 'username').send_keys("CINDYCLAUDIA123@GMAIL.COM")              
    sleep(2)                                                                
end

And("I fill invalid password “12345” to field password") do
    driver.find_element(:id, 'password').send_keys("12345")             
    sleep(2)                                                                
end

And("I fill valid password “qwerty” to field password") do
    driver.find_element(:id, 'password').send_keys("qwerty")             
    sleep(2)                                                                
end

Then("I click Login button") do
    driver.find_element(:id, 'email-login-button').click()          
    sleep(5)
    driver.close()                                               
end
