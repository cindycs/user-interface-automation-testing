require 'selenium-webdriver'
require 'rubygems'
require 'rspec'

driver = Selenium::WebDriver.for:chrome

Given("I open the Login page") do
    driver.navigate.to "https://stockbit.com/login"
  end

When("I click the Sign Up link") do
    driver.find_element(:xpath, '//*[@id="__next"]/div/div[3]/div/p/a').click()
    sleep(2)
end

And("I click the Registration with googlee") do
    driver.find_element(:id, 'google-register').click()
    sleep(2)
end

And("I input “cindyclaudee37@gmail.com” to the email") do
    driver.find_element(:id, 'identifierId').send_keys("cindyclaudee37@gmail.com")
    sleep(2)
end

And("I click the Berikutnya button") do
    driver.find_element(:xpath, '//*[@id="identifierNext"]/div/button"]').click()
    sleep(2)
end

And("I input “cindyclaudee” to the password") do
    driver.find_element(:xpath, '//*[@id="password"]/div[1]/div/div[1]/input').send_keys("cindyclaudee")
    sleep(2)
end

Then("I click the next button") do
    driver.find_element(:xpath, '//*[@id="passwordNext"]/div/button').click()          
    sleep(5) 
end

When("I fill “Cindy Claudia S” to field name") do
    driver.find_element(:id, 'fullname').send_keys("Cindy Claudia S")             
    sleep(2) 
end                                                                  

And("I fill “1234” to field username") do
    driver.find_element(:id, 'username').send_keys("1234")             
    sleep(2) 
end

Then("I click the Register button") do
    driver.find_element(:id, 'register-button').click()          
    sleep(5)                                                                                 
end

When("I fill “81178345623213” to field phone number") do
    driver.find_element(:xpath, '//*[@id="__next"]/div/div[3]/div/div[2]/input').send_keys("“12345”")             
    sleep(2) 
end

Then("I click the Send Verification Code button") do
    driver.find_element(:id, 'verify-phone').click()          
    sleep(5) 
end
